package demo.rcp.client.preference;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.jface.preference.StringFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import demo.rcp.client.Activator;

public class DefaultPreferencesPage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {

	public static final String USER_NAME = "demo.rcp.client.preference.user.name";
	public static final String SERVER_ADDRESS = "demo.rcp.client.preference.server.address";
	public static final String SERVER_PORT = "demo.rcp.client.preference.server.port";

	public DefaultPreferencesPage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		addField(new StringFieldEditor(USER_NAME, "User Name:", getFieldEditorParent()));
		addField(new StringFieldEditor(SERVER_ADDRESS, "Server Address:", getFieldEditorParent()));
		addField(new IntegerFieldEditor(SERVER_PORT, "Port:", getFieldEditorParent()));
	}

}
