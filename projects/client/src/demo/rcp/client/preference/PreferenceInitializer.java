package demo.rcp.client.preference;

import org.eclipse.core.runtime.preferences.AbstractPreferenceInitializer;
import org.eclipse.jface.preference.IPreferenceStore;

import demo.rcp.client.Activator;

public class PreferenceInitializer extends AbstractPreferenceInitializer {

	public PreferenceInitializer() {
	}

	@Override
	public void initializeDefaultPreferences() {
		IPreferenceStore store = Activator.getDefault().getPreferenceStore();
		store.setDefault(DefaultPreferencesPage.SERVER_ADDRESS, "127.0.0.1");
		store.setDefault(DefaultPreferencesPage.SERVER_PORT, "5555");
	}

}
