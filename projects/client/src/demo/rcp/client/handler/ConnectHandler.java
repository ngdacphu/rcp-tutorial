package demo.rcp.client.handler;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.dialogs.ErrorDialog;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.progress.UIJob;

import demo.rcp.client.Activator;
import demo.rcp.client.Application;
import demo.rcp.client.preference.DefaultPreferencesPage;
import demo.rcp.client.view.ChatRoomView;

public class ConnectHandler extends AbstractHandler {

	public ConnectHandler() {
	}

	@Override
	public Object execute(ExecutionEvent event) throws ExecutionException {
		try {
			HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(ChatRoomView.ID);
		} catch (PartInitException e1) {
			e1.printStackTrace();
		}
		IPreferenceStore preferenceStore = Activator.getDefault().getPreferenceStore();
		String userName = preferenceStore.getString(DefaultPreferencesPage.USER_NAME);
		if (userName == null || userName.trim().isEmpty()) {
			MessageDialog.openError(HandlerUtil.getActiveShell(event), "Error", "You should provide a name in the Server Settings");
			return null;
		}
		Job job = new Job("ConnectToServer") {

			@Override
			protected IStatus run(IProgressMonitor monitor) {
				monitor.beginTask("Connecting...", 100);
				monitor.worked(10);
				try {
					Application.getService().connect();
					monitor.done();
					UIJob uiJob = new UIJob("UpdateUI") {

						@Override
						public IStatus runInUIThread(IProgressMonitor monitor) {
							try {
								HandlerUtil.getActiveWorkbenchWindow(event).getActivePage().showView(ChatRoomView.ID);
							} catch (PartInitException e) {
								e.printStackTrace();
							}
							return Status.OK_STATUS;
						}
					};
					uiJob.schedule();
					return Status.OK_STATUS;
				} catch (Exception ex) {
					ex.printStackTrace();
					return Status.CANCEL_STATUS;
				} finally {
					monitor.done();
				}
			}
		};
		job.setUser(true);
		job.schedule();
		return null;
	}

}
