package demo.rcp.client.handler.service.listener;

public interface ServiceListener {
	void onNewMessage(String message);
}
