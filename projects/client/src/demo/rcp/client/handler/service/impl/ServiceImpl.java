package demo.rcp.client.handler.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.eclipse.jface.preference.IPreferenceStore;
import org.json.JSONException;
import org.json.JSONObject;

import demo.rcp.client.Activator;
import demo.rcp.client.handler.service.Service;
import demo.rcp.client.handler.service.listener.ServiceListener;
import demo.rcp.client.preference.DefaultPreferencesPage;

public class ServiceImpl implements Service, Runnable {

	private BufferedReader reader;
	private BufferedWriter writer;
	private Socket socket;
	private List<ServiceListener> listeners = new ArrayList<ServiceListener>();
	private String username;

	@Override
	public void connect() throws UnknownHostException, IOException {
		IPreferenceStore prefStore = Activator.getDefault().getPreferenceStore();
		String serverHost = prefStore.getString(DefaultPreferencesPage.SERVER_ADDRESS);
		int serverPort = prefStore.getInt(DefaultPreferencesPage.SERVER_PORT);
		username = prefStore.getString(DefaultPreferencesPage.USER_NAME);
		System.out.println("Connecting to " + serverHost + ":" + serverPort);
		socket = new Socket(serverHost, serverPort);
		reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
		writer = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
		writer.write(username);
		writer.newLine();
		writer.flush();
		String line = reader.readLine();
		try {
			JSONObject obj = new JSONObject(line);
			if ("CONNECTION".equals(obj.getString("type")) && "Approved".equals(obj.getString("message"))
					&& "System".equals(obj.getString("from"))) {
				System.out.println("Connection granted!");
			} else {
				throw new IOException("Connection failed. Server response: " + obj);
			}
		} catch (JSONException e) {
			e.printStackTrace();
			System.out.println(line);
		}
		new Thread(this).start();
	}

	@Override
	public void disconnect() {
		if (socket != null) {
			IOUtils.closeQuietly(socket);
			socket = null;
		}

		if (reader != null) {
			IOUtils.closeQuietly(reader);
		}

		if (writer != null) {
			IOUtils.closeQuietly(writer);
		}
	}

	@Override
	public void addListener(ServiceListener listener) {
		listeners.add(listener);
	}

	@Override
	public void run() {
		while (true) {
			try {
				String line = reader.readLine();
				System.out.println(line);
				synchronized (listeners) {
					for (ServiceListener listener : listeners) {
						listener.onNewMessage(line);
					}
				}

			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}

	@Override
	public void sendMessage(String text) throws Exception {
		JSONObject obj = new JSONObject();
		obj.put("type", "MESSAGE");
		obj.put("from", username);
		obj.put("message", text);
		writer.write(obj.toString());
		writer.newLine();
		writer.flush();
	}

}
