package demo.rcp.client.handler.service;

import java.io.IOException;
import java.net.UnknownHostException;

import demo.rcp.client.handler.service.listener.ServiceListener;

public interface Service {

	void connect() throws UnknownHostException, IOException;

	void disconnect();
	
	void addListener(ServiceListener listener) ;

	void sendMessage(String text) throws Exception;
}
