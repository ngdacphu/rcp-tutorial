package demo.rcp.client.view;

import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.ui.part.ViewPart;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Text;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.GridData;
import org.json.JSONException;
import org.json.JSONObject;

import demo.rcp.client.Application;
import demo.rcp.client.handler.service.listener.ServiceListener;

import org.eclipse.swt.widgets.List;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ListViewer;
import org.eclipse.jface.viewers.Viewer;

public class ChatRoomView extends ViewPart implements ServiceListener {
	public ChatRoomView() {
		Application.getService().addListener(this);
	}

	public static final String ID = "demo.rcp.client.view.ChatRoomView";
	private Text text;
	private List contentList;

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(new FormLayout());

		text = new Text(parent, SWT.BORDER);
		FormData fd_text = new FormData();
		fd_text.left = new FormAttachment(0, 10);
		fd_text.top = new FormAttachment(100, -35);
		fd_text.bottom = new FormAttachment(100, -10);
		text.setLayoutData(fd_text);

		Button btnSend = new Button(parent, SWT.NONE);
		fd_text.right = new FormAttachment(100, -103);
		FormData fd_btnSend = new FormData();
		fd_btnSend.top = new FormAttachment(0, 434);
		fd_btnSend.left = new FormAttachment(text, 6);
		fd_btnSend.right = new FormAttachment(100, -10);
		btnSend.setLayoutData(fd_btnSend);
		btnSend.setText("Send!");
		
		contentList = new List(parent, SWT.BORDER | SWT.H_SCROLL | SWT.V_SCROLL);
		FormData fd_list = new FormData();
		fd_list.top = new FormAttachment(text, -424, SWT.TOP);
		fd_list.right = new FormAttachment(btnSend, 0, SWT.RIGHT);
		fd_list.bottom = new FormAttachment(text, -6);
		fd_list.left = new FormAttachment(0, 10);
		contentList.setLayoutData(fd_list);
		
		btnSend.addListener(SWT.Selection, new Listener() {
			
			@Override
			public void handleEvent(Event event) {
				try {
					Application.getService().sendMessage(text.getText());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

	@Override
	public void setFocus() {

	}
	
	@Override
	public void onNewMessage(String message) {
		UIJob job = new UIJob("UpdateMessageList") {

			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				try {
					JSONObject obj = new JSONObject(message);
					contentList.add(obj.getString("from") + ": " + obj.getString("message"));
				} catch (JSONException e) {
					e.printStackTrace();
					contentList.add(message);
				}
				return Status.OK_STATUS;
			}
		};
		job.schedule();

	}
}