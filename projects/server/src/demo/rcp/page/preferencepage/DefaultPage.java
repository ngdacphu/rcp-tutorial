package demo.rcp.page.preferencepage;

import org.eclipse.jface.preference.FieldEditorPreferencePage;
import org.eclipse.jface.preference.IntegerFieldEditor;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import demo.rcp.activator.Activator;

public class DefaultPage extends FieldEditorPreferencePage implements
		IWorkbenchPreferencePage {

	public DefaultPage() {
		super(GRID);
	}

	@Override
	public void init(IWorkbench workbench) {
		setPreferenceStore(Activator.getDefault().getPreferenceStore());
	}

	@Override
	protected void createFieldEditors() {
		addField(new IntegerFieldEditor(PrefConst.SERVER_PORT, "Listen Port:",
				getFieldEditorParent()));
	}

}
