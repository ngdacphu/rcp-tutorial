package demo.rcp.service;

import java.util.List;

import demo.rcp.service.listener.ServiceListener;
import demo.rcp.service.model.Client;

public interface Service {

	public void startServer();

	public void restartServer();

	public void stopServer();

	public void addServiceListner(ServiceListener listener);

	public List<Client> getClientList();

}