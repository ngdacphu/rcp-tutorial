package demo.rcp.service.impl;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.ui.progress.UIJob;
import org.json.JSONException;
import org.json.JSONObject;

import demo.rcp.activator.Activator;
import demo.rcp.page.preferencepage.PrefConst;
import demo.rcp.service.Service;
import demo.rcp.service.listener.ServiceListener;
import demo.rcp.service.model.Client;

public class ServiceImpl implements Service {

	private ServerSocket serverSocket = null;
	private List<Client> clientList = new ArrayList<Client>();
	private List<ServiceListener> listeners = new ArrayList<ServiceListener>();

	public ServiceImpl() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see demo.rcp.service.IService#startServer()
	 */
	@Override
	public void startServer() {
		stopServer();
		int port = Activator.getDefault().getPreferenceStore().getInt(PrefConst.SERVER_PORT);
		System.out.println("Starting server on port: " + port);
		try {
			serverSocket = new ServerSocket(port);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Wating for client...");

		Thread serverThread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						final Socket clientSocket = serverSocket.accept();
						Thread clientThread = new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									BufferedReader br = new BufferedReader(new InputStreamReader(
											clientSocket.getInputStream()));
									String name = br.readLine();
									final Client client = new Client(name, clientSocket);
									UIJob uiJob = new UIJob("UpdateClientList") {

										@Override
										public IStatus runInUIThread(IProgressMonitor monitor) {
											synchronized (clientList) {
												clientList.add(client);
												synchronized (listeners) {
													for (ServiceListener listener : listeners) {
														listener.onClientListUpdated();
													}
												}
											}
											return Status.OK_STATUS;
										}
									};
									uiJob.schedule();

									BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
											clientSocket.getOutputStream()));
									JSONObject obj = new JSONObject();
									try {
										obj.put("type", "CONNECTION");
										obj.put("from", "System");
										obj.put("message", "Approved");
									} catch (JSONException e) {
									}
									bw.write(obj.toString());
									bw.newLine();
									bw.flush();
									JSONObject welcomeMsg = new JSONObject();
									try {
										welcomeMsg.put("type", "MESSAGE");
										welcomeMsg.put("from", "System");
										welcomeMsg.put("message", "Welcome! " + client.getName());
									} catch (Exception ex) {

									}
									bw.write(welcomeMsg.toString());
									bw.newLine();
									bw.flush();
									while (true) {
										String line = br.readLine();
										broadcastMessage(line);
									}
								} catch (IOException e) {
									e.printStackTrace();
								}
							}
						});
						clientThread.start();
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}
			}
		});
		serverThread.start();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see demo.rcp.service.IService#restartServer()
	 */
	@Override
	public void restartServer() {
		stopServer();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		startServer();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see demo.rcp.service.IService#stopServer()
	 */
	@Override
	public void stopServer() {
		try {
			if (serverSocket != null) {
				IOUtils.closeQuietly(serverSocket);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	@Override
	public void addServiceListner(ServiceListener listener) {
		synchronized (listeners) {
			this.listeners.add(listener);
		}
	}

	@Override
	public List<Client> getClientList() {
		return clientList;
	}

	private void broadcastMessage(String message) {
		synchronized (clientList) {
			for (Client client : clientList) {
				try {
					BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(client.getSocket().getOutputStream()));
					bw.write(message);
					bw.newLine();
					bw.flush();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}
}
