package demo.rcp.view;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ColumnLabelProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.ui.part.ViewPart;

import demo.rcp.Application;
import demo.rcp.service.listener.ServiceListener;
import demo.rcp.service.model.Client;

public class ClientsListView extends ViewPart implements ServiceListener {

	public static final String ID = "demo.rpc.view.ClientsView";
	private Table table;
	private TableViewer tableViewer;

	public ClientsListView() {
		Application.getService().addServiceListner(this);
		
	}

	@Override
	public void createPartControl(Composite parent) {
		parent.setLayout(null);

		tableViewer = new TableViewer(parent, SWT.BORDER | SWT.FULL_SELECTION);
		table = tableViewer.getTable();
		table.setBounds(10, 10, 574, 449);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		tableViewer.setContentProvider(new ArrayContentProvider());
		tableViewer.setInput(Application.getService().getClientList());
		
		TableViewerColumn nameColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		nameColumn.getColumn().setWidth(100);
		nameColumn.getColumn().setText("Name");
		nameColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Client client = (Client) element;
				return client.getName();
			}
		});

		TableViewerColumn addressColumn = new TableViewerColumn(tableViewer, SWT.NONE);
		addressColumn.getColumn().setWidth(100);
		addressColumn.getColumn().setText("IP Address");
		addressColumn.setLabelProvider(new ColumnLabelProvider() {
			@Override
			public String getText(Object element) {
				Client client = (Client) element;
				return client.getSocket().getInetAddress().getHostAddress();
			}
		});
		
	}

	@Override
	public void setFocus() {

	}

	@Override
	public void onClientListUpdated() {
		System.out.println("Client list updated!");
		tableViewer.setInput(Application.getService().getClientList());
		tableViewer.refresh();
	}
}
